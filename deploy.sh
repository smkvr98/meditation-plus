#!/bin/bash

SSH_PRIVATE_KEY=$1
USER=$2
SERVER=$3
FOLDER=$4
VERSION=$5

set -e

# Preparing Server
rm -f server/dist/src/config/config.json
cp -R server/node_modules server/dist/node_modules
cd server
npm --no-git-tag-version version "$VERSION"
cp package.json dist/package.json
cd ..

# Preparing Client
touch client/dist/assets/version.json
echo "{ \"version\": \"$VERSION\" }" > client/dist/assets/version.json

# https://docs.gitlab.com/ee/ci/ssh_keys/README.html
eval $(ssh-agent -s)
mkdir -p ~/.ssh
touch ~/.ssh/id_rsa.gitlab ~/.ssh/config
echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa.gitlab
chmod 600 ~/.ssh/id_rsa.gitlab
ssh-add ~/.ssh/id_rsa.gitlab
printf "Host *\n\tStrictHostKeyChecking no\n\tIdentityFile ~/.ssh/id_rsa.gitlab\n\tIdentitiesOnly yes\n\n" > ~/.ssh/config

# Deploying Server
tar -czf transfer.tgz server/dist
scp -o "StrictHostKeyChecking no" transfer.tgz $USER@$SERVER:$FOLDER
ssh -o "StrictHostKeyChecking no" $USER@$SERVER "cd $FOLDER; cp config/config.json ./; cp config/firebase.json ./; rm -rf app .babelrc config scripts server.conf.js  server.js sockets node_modules package.json; tar -xzf transfer.tgz --strip 2; mv config.json config/config.json; mv firebase.json config/firebase.json; rm transfer.tgz"

# Deploying Client
tar -czf transfer-client.tgz client/dist
scp -o "StrictHostKeyChecking no" transfer-client.tgz $USER@$SERVER:$FOLDER
ssh -o "StrictHostKeyChecking no" $USER@$SERVER "cd $FOLDER; rm -rf client; mkdir client; tar -xzf transfer-client.tgz -C client --strip 2; rm transfer-client.tgz"