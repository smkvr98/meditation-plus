import MeetingMessage from '../models/meeting-message.model.js';
import Meeting from '../models/meeting.model.js';
let ObjectId = require('mongoose').Types.ObjectId;
import moment from 'moment-timezone';
import push from '../helper/push.js';
import { logger } from '../helper/logger.js';
import { isYuttadhammo } from '../helper/appointment';
import _ from 'lodash';

/**
 * Populates the messages in a list of meeting messages, which
 * are marked as the first one in the meeting, with more meeting
 * information.
 *
 * @param {MeetinMessage[]} messages List of messages
 */
const populateFirstMessages = async (messages) => {
  for (let i = 0; i < messages.length; i++) {
    if (messages[i].first === true) {
      const meeting = await Meeting
        .findById(messages[i].meeting, 'createdAt user')
        .populate('user', 'name username');

      _.set(messages, `${i}.meeting`, meeting);
    }
  }
};

export default (app, router, io) => {

  /**
   * Broadcasts a socket event to the participants of a meeting.
   *
   * @param {Meeting} meeting   the `req.meeting` object with the corresponding meeting
   * @param {User}    user      the `req.user` object of the request
   * @param {string}  eventname socket event name
   * @param {object}  payload   socket payload object to emit
   */
  const ioMeetingBroadcast = (meeting, eventname, payload) => {
    const userId = meeting.user.toString();

    Object.values(io.sockets.sockets)
      .filter(socket => !!socket.decoded_token)
      .filter(({ decoded_token }) => decoded_token._id === userId
        || isYuttadhammo(decoded_token)
      )
      .map(socket => socket.emit(eventname, payload));
  };

  /**
   * When meeting id is given, validate the id and
   * set req.message to the corresponding meeting.
   */
  router.param('meetingId', async (req, res, next, meetingId) => {
    try {
      req.meeting = null;

      if (!ObjectId.isValid(meetingId)) {
        return res.status(400).json({ error: 'ID is not valid.' });
      }

      const meeting = await Meeting
        .findOne({ _id: ObjectId(meetingId) })
        .lean()
        .then();

      if (!meeting) {
        return res.status(400).json({ error: 'Meeting not found.' });
      }

      if (!isYuttadhammo(req.user) && (meeting.user.toString() !== req.user._id
        || moment().subtract(1, 'hour').isAfter(meeting.createdAt))) {
        return res.status(403).json({ error: 'Unauthorized.' });
      }

      req.meeting = meeting;

      next();
    } catch (err) {
      logger.error(req.url, 'VALIDATE-MEETING', err);
      return res.status(500).json({ error: 'Internal server error.' });
    }
  });

  /**
   * @api {get} /api/meeting-message/meetings Get chat history of all meetings of a user
   * @apiName ListMessages
   * @apiGroup Message
   *
   * @apiSuccess {Object[]} messages           List of messages.
   * @apiSuccess {String}   messages.text      Message body
   * @apiSuccess {Date}     messages.createdAt Date of creation
   * @apiSuccess {User}     messages.user      The posting user
   */
  router.get('/api/meeting-message', async (req, res) => {
    try {
      const msgPerPage = 50;
      const page = req.query.page || 0;

      let criteria = {};
      if (!isYuttadhammo(req.user)) {
        // get all meetings of user (expected to not be a big amount)
        // in order to filter messages to return and later return meetings
        const ownMeetings = await Meeting.find({ user: req.user._id }).select('_id');

        criteria = {
          meeting: { $in: ownMeetings.map(({ _id }) => _id) },
          deleted: { $ne: true }
        };
      }

      let messages = await MeetingMessage
        .find(criteria)
        .sort([['createdAt', 'descending']])
        .skip(msgPerPage * page)
        .limit(msgPerPage)
        .populate('user', 'name avatarImageToken lastMeditation country username')
        .lean()
        .then();

      messages.reverse();

      // populate first message of meeting with meeting information
      await populateFirstMessages(messages);

      res.json(messages);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/meeting-message/synchronize Fetches meeting messages for a specific timeframe
   * @apiName SynchronizeMeetingMessages
   * @apiGroup Message
   *
   * @apiSuccess {Object[]} messages           List of messages.
   * @apiSuccess {String}   messages.text      Message body
   * @apiSuccess {Date}     messages.createdAt Date of creation
   * @apiSuccess {User}     messages.user      The posting user
   */
  router.post('/api/meeting-message/synchronize', async (req, res) => {
    try {
      const timeFrameStart = moment(req.body.timeFrameStart);
      const timeFrameEnd = moment(req.body.timeFrameEnd);

      let criteria = {
        createdAt: {
          $gt: timeFrameStart,
          $lte: timeFrameEnd
        },
        deleted: { $ne: true },
        private: { $ne: true }
      };

      if (isYuttadhammo(req.user)) {
        delete criteria['deleted'];
      } else {
        // get all meetings of user (expected to not be a big amount)
        // in order to filter messages to return and later return meetings
        const ownMeetings = await Meeting.find({ user: req.user._id }).select('_id');

        criteria = {
          ...criteria,
          meeting: { $in: ownMeetings.map(({ _id }) => _id) }
        };
      }

      let messages = await MeetingMessage
        .find(criteria)
        .sort([['createdAt', 'descending']])
        .populate('user', 'name avatarImageToken lastMeditation country username')
        .lean()
        .then();

      messages.reverse();

      // populate first message of meeting with meeting information
      await populateFirstMessages(messages);

      res.json(req.body.countOnly && req.body.countOnly === true ? messages.length : messages);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {post} /api/meeting-message Post a new meeting message
   * @apiName AddMeetingMessage
   * @apiGroup MeetingMessage
   *
   * @apiParam {String} text Message body
   */
  router.post('/api/meeting-message/:meetingId', async (req, res) => {
    try {
      const messageText = req.body.text ? req.body.text : '';

      if (messageText.length > 1000) {
        return res.status(400).json('The message text exceeds the limit of 1000 characters.');
      }

      // fetch previous message to detect missed timespans on the client
      const previousMessage = await MeetingMessage
        .findOne({
          private: { $ne: true },
          meeting: req.meeting._id
        })
        .sort('-createdAt')
        .then();

      let message = await MeetingMessage
        .create({
          text: messageText,
          user: req.user,
          meeting: req.meeting._id,
          first: !previousMessage
        });

      // add user details for response and broadcast
      let populated = await message.populate(
        'user',
        'name avatarImageToken country lastMeditation username'
      ).execPopulate();

      // add more information to meeting object if message is the first one
      await populateFirstMessages([populated]);

      let socketPayload = {
        previous: previousMessage,
        current: populated
      };

      ioMeetingBroadcast(req.meeting, 'meeting-message', socketPayload);

      res.json(populated);

      push.send({
        'notifications.message': true,
        ...(isYuttadhammo(req.user)
          ? { _id: req.meeting.user }
          : { username: 'yuttadhammo' }
        )
      }, {
        notification: {
          title: req.user.name || 'New Message in Meeting',
          body: messageText
        },
        webpush: {
          fcm_option: {
            url: '/meet'
          }
        }
      });
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });

  /**
   * @api {put} /api/message/:id Edit new message
   * @apiName EditMessage
   * @apiGroup Message
   *
   * @apiParam {String} text Message body
   */
  router.put('/api/meeting-message/:id/:meetingId', async (req, res) => {
    try {
      let message = await MeetingMessage
        .findOne({
          _id: ObjectId(req.params.id),
          meeting: req.meeting._id
        })
        .exec();

      if (!message) return res.sendStatus(404);

      // only allow to edit own messages (or being admin)
      if (message.user != req.user._id &&
        req.user.role !== 'ROLE_ADMIN') {
        return res.sendStatus(400);
      }

      // only allow to edit messages for 30 minutes
      if (req.user.role !== 'ROLE_ADMIN') {
        const created = moment(message.createdAt);
        const now = moment();
        const duration = moment.duration(now.diff(created));

        if (duration.asMinutes() >= 30) {
          return res.status(400).json({
            error: 'Only allowed to edit message within 30 minutes.'
          });
        }
      }

      message.text = req.body.text;
      message.edited = true;
      await message.save();

      // add user details for response and broadcast
      let populated = await message.populate(
        'user',
        'name avatarImageToken country lastMeditation username'
      ).execPopulate();

      ioMeetingBroadcast(req.meeting, 'meeting-message-update', { populated });

      res.json(populated);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });

  /**
   * @api {delete} /api/message/:id Delete message
   * @apiName DeleteMessage
   * @apiGroup Message
   */
  router.delete('/api/meeting-message/:id/:meetingId', async (req, res) => {
    try {
      let message = await MeetingMessage
        .findOne({
          _id: ObjectId(req.params.id),
          meeting: req.meeting._id
        })
        .exec();

      if (!message) return res.sendStatus(404);

      // only allow to edit own messages (or being admin)
      if (message.user != req.user._id &&
        req.user.role !== 'ROLE_ADMIN') {
        return res.sendStatus(400);
      }

      message.deleted = true;
      await message.save();

      // add user details for response and broadcast
      let populated = await message.populate(
        'user',
        'name avatarImageToken country lastMeditation username'
      ).execPopulate();

      ioMeetingBroadcast(req.meeting, 'meeting-message-update', { populated });

      res.json(populated);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });
};
