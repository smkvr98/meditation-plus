import { app } from '../../server.conf.js';
import supertest from 'supertest';
import { expect } from 'chai';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import MeetingMessage from '../models/meeting-message.model.js';
import Meeting from '../models/meeting.model.js';
import Appointment from '../models/appointment.model';
import moment from 'moment-timezone';
import timekeeper from 'timekeeper';
import _ from 'lodash';

const request = supertest(app);
let user = new AuthedSupertest();
let user2 = new AuthedSupertest(
  'Second User',
  'user2',
  'user2@sirimangalo.org',
  'password'
);
let user3 = new AuthedSupertest(
  'Third User',
  'user3',
  'user3@sirimangalo.org',
  'password'
);
let admin = new AuthedSupertest(
  'yuttadhammo',
  'yuttadhammo',
  'yuttadhammo@sirimangalo.org',
  'password',
  'ROLE_ADMIN'
);

const cleanup = async () => {
  await Appointment.deleteMany({});
  await Meeting.deleteMany({});
  await MeetingMessage.deleteMany({});
  timekeeper.reset();
};

const testMessages1 = [
  { text: 'Hello, Im the normal user2', first: true },
  { text: 'Hi, Im the teacher/admin' },
  { text: 'Ok!' },
  { text: 'Then this information is only for user2: https://www.youtube.com/watch?v=QFqdlfeq7wE' },
  { text: 'afhaskdjöfaskjdföas', deleted: true }
];

const testMessages2 = [
  { text: 'Hi, it is me, user3', first: true },
  { text: 'Hi there, this is the admin' },
  { text: 'All right!', edited: true },
  { text: 'Here is your private information: https://www.youtube.com/watch?v=8eUYjlEL9iM' },
  { text: 'Thanks!' }
];

const assertMessages = (resMessages, testMessages, user) => {
  const isAdmin = user.user.username === 'yuttadhammo';
  const expectedMessages = testMessages.filter(m => isAdmin || !m.deleted);
  expect(resMessages.length).to.equal(expectedMessages.length);

  for (let i = 0; i < expectedMessages.length; i++) {
    expect(_.difference( ['text', 'user', 'meeting'], _.keys(resMessages[i])).length).to.equal(0);
    expect(_.isMatch(resMessages[i], expectedMessages[i])).to.be.true;


    if (resMessages[i].first === true) {
      expect(resMessages[i].meeting).to.have.keys(['_id', 'createdAt', 'user']);
    } else {
      expect(resMessages[i].meeting).to.be.a('string');
    }

    if (!isAdmin) {
      expect([admin.user._id.toString(), user.user._id.toString()]).to.include(resMessages[i].user._id);
    }
  }
};

describe('Meeting Message Routes', () => {
  let appointment1, appointment2;
  let meeting1, meeting2, meeting3;

  user.authorize();
  user2.authorize();
  user3.authorize();
  admin.authorize();

  beforeEach(async () => {
    await cleanup();

    appointment1 = await Appointment.create({ hour: 15, weekDay: 3, user: user2.user._id });
    appointment2 = await Appointment.create({ hour: 17, day: 3, user: user3.user._id });

    timekeeper.travel('2020-02-25T15:00:30Z');
    meeting1 = await Meeting.create({
      user: user2.user._id,
      appointment: appointment1
    });

    timekeeper.travel('2020-02-25T19:00:30Z');
    meeting3 = await Meeting.create({
      user: user3.user._id,
      appointment: appointment2
    });

    timekeeper.travel('2020-02-25T17:00:30Z');
    meeting2 = await Meeting.create({
      user: user3.user._id,
      appointment: appointment2
    });

    // create test messages for both meetings
    for (let i = 0; i < testMessages1.length; i++) {
      timekeeper.travel(moment('2020-02-25T15:00:30Z').add(i, 'minutes').format());
      await MeetingMessage.create({
        ...testMessages1[i],
        meeting: meeting1._id,
        user: i % 2 === 0 ? user2.user._id : admin.user._id
      });
    }

    for (let i = 0; i < testMessages2.length; i++) {
      timekeeper.travel(moment('2020-02-25T17:00:30Z').add(i, 'minutes').format());
      await MeetingMessage.create({
        ...testMessages2[i],
        meeting: meeting2._id,
        user: i % 2 === 0 ? user3.user._id : admin.user._id
      });
    }
  });

  afterEach(cleanup);

  describe('GET /api/meeting-message', () => {
    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/meeting-message')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with no data when user has no meeting messages', done => {
      user
        .get('/api/meeting-message')
        .expect(200)
        .end((err, res) => {
          expect(res.body.length).to.equal(0);
          done(err);
        });
    });

    it('should respond with only message from own meeting for normal user (1)', done => {
      user2
        .get('/api/meeting-message')
        .expect(200)
        .end((err, res) => {
          assertMessages(res.body, testMessages1, user2);
          done(err);
        });
    });

    it('should respond with only message from own meeting for normal user (2)', done => {
      user3
        .get('/api/meeting-message')
        .expect(200)
        .end((err, res) => {
          assertMessages(res.body, testMessages2, user3);
          done(err);
        });
    });

    it('should respond with all messages from own meeting for admin user', done => {
      admin
        .get('/api/meeting-message')
        .expect(200)
        .end((err, res) => {
          assertMessages(res.body, [...testMessages1, ...testMessages2], admin);
          done(err);
        });
    });

    it('should respond with more data when added', done => {
      let newMessage = {
        text: 'Another message',
        user: admin.user._id,
        meeting: meeting2._id.toString()
      };
      MeetingMessage.create(newMessage).then((res, err) => {
        if (err) return done(err);

        admin
          .get('/api/meeting-message')
          .expect(200)
          .end((err, res) => {
            assertMessages(res.body, [
              ...testMessages1,
              ...testMessages2,
              _.omit(newMessage, ['user'])
            ], admin);
            done(err);
          });
      });
    });
  });

  describe('POST /api/meeting-message/synchronize', () => {
    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/meeting-message/synchronize')
        .expect(401)
        .end(err => done(err));
    });

    it('should not respond with messages when no meetings', done => {
      user
        .post('/api/meeting-message/synchronize')
        .send({
          timeFrameStart: moment('2020-02-25T10:00:30Z'),
          timeFrameEnd: moment()
        })
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.length).to.equal(0);
          done();
        });
    });

    it('should respond with correct messages (1)', done => {
      user2
        .post('/api/meeting-message/synchronize')
        .send({
          timeFrameStart: moment('2020-02-25T15:02:00Z'),
          timeFrameEnd: moment()
        })
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          assertMessages(res.body, testMessages1.slice(2), user2);
          done();
        });
    });

    it('should respond with correct messages (2)', done => {
      user3
        .post('/api/meeting-message/synchronize')
        .send({
          timeFrameStart: moment('2020-02-25T14:00:00Z'),
          timeFrameEnd: moment('2020-02-25T17:04:00Z')
        })
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          assertMessages(res.body, testMessages2.slice(0, -1), user3);
          done();
        });
    });

    it('should respond with correct messages (3)', done => {
      admin
        .post('/api/meeting-message/synchronize')
        .send({
          timeFrameStart: moment('2020-02-25T15:01:00Z'),
          timeFrameEnd: moment('2020-02-25T17:03:00Z')
        })
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          assertMessages(
            res.body,
            [...testMessages1.slice(1), ...testMessages2.slice(0, -2)],
            admin
          );
          done();
        });
    });
  });

  describe('POST /api/meeting-message/:meeting', () => {
    it('should respond with 401 when not authenticated', done => {
      timekeeper.travel('2020-02-25T15:01:30Z');
      request
        .post('/api/meeting-message/' + meeting1._id)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 if id is invalid', done => {
      user2
        .post('/api/meeting-message/invalid')
        .send({ text: 'My Text' })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 if id does not exist', done => {
      user2
        .post('/api/meeting-message/5e55539eb6d9462583479ca6')
        .send({ text: 'My Text' })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 403 when not authenticated for meeting (1)', done => {
      timekeeper.travel('2020-02-25T17:05:30Z');
      user2
        .post(`/api/meeting-message/${meeting2._id}`)
        .send({ text: 'My Text' })
        .expect(403)
        .end(err => done(err));
    });

    it('should respond with 403 when not authenticated for meeting (2)', done => {
      timekeeper.travel('2020-02-25T15:01:30Z');
      user3
        .post(`/api/meeting-message/${meeting1._id}`)
        .send({ text: 'My Text' })
        .expect(403)
        .end(err => done(err));
    });

    it('should respond with 403 when too late', done => {
      timekeeper.travel('2020-02-25T16:30:30Z');
      user2
        .post(`/api/meeting-message/${meeting1._id}`)
        .send({ text: 'My Text' })
        .expect(403)
        .end(err => done(err));
    });

    it('should save message when authenticated (1)', done => {
      timekeeper.travel('2020-02-25T15:30:30Z');

      let newMessage = { text: 'My Text' };

      user2
        .post(`/api/meeting-message/${meeting1._id}`)
        .send(newMessage)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.text).to.equal(newMessage.text);
          expect(res.body.user._id).to.equal(user2.user._id.toString());

          done();
        });
    });

    it('should set first property correctly', (done) => {
      timekeeper.travel('2020-02-25T19:00:30Z');

      let newMessage = { text: 'My Text 2' };

      user3
        .post(`/api/meeting-message/${meeting3._id}`)
        .send(newMessage)
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);
          expect(res.body.text).to.equal(newMessage.text);
          expect(res.body.user._id).to.equal(user3.user._id.toString());
          expect(res.body.first).to.be.true;
          expect(res.body.meeting._id).to.equal(meeting3._id.toString());
          //expect(res.body.meeting.createdAt).to.equal(moment(meeting3.createdAt).format());
          done();
        });
    });
  });

  // describe('PUT /api/message/:id', () => {
  //   it('should respond with 401 when not authenticated', done => {
  //     request
  //       .put(`/api/meeting-message/${message._id}`)
  //       .expect(401)
  //       .end(err => done(err));
  //   });

  //   it('should respond with 400 when not allowed', done => {
  //     user2
  //       .put(`/api/meeting-message/${message._id}`)
  //       .expect(401)
  //       .end(err => done(err));
  //   });

  //   it('should update message when owner', done => {
  //     user
  //       .put(`/api/meeting-message/${message._id}`)
  //       .send({ text: 'My Text 2' })
  //       .expect(200)
  //       .end((err, res) => {
  //         if (err) return done(err);
  //         expect(res.body.text).to.equal('My Text 2');
  //         expect(res.body.edited).to.equal(true);
  //         done();
  //       });
  //   });

  //   it('should update message when admin', done => {
  //     admin
  //       .put(`/api/meeting-message/${message._id}`)
  //       .send({ text: 'My Text 2' })
  //       .expect(200)
  //       .end((err, res) => {
  //         if (err) return done(err);
  //         expect(res.body.text).to.equal('My Text 2');
  //         expect(res.body.edited).to.equal(true);
  //         done();
  //       });
  //   });
  // });

  // describe('DELETE /api/message/:id', () => {
  //   it('should respond with 401 when not authenticated', done => {
  //     request
  //       .delete(`/api/message/${message._id}`)
  //       .expect(401)
  //       .end(err => done(err));
  //   });

  //   it('should respond with 400 when now allowed', done => {
  //     user2
  //       .delete(`/api/message/${message._id}`)
  //       .expect(401)
  //       .end(err => done(err));
  //   });

  //   it('should correctly delete when authenticated as admin', done => {
  //     admin
  //       .delete(`/api/message/${message._id}`)
  //       .expect(200)
  //       .end(err => done(err));
  //   });

  //   it('should correctly delete when owner', done => {
  //     user
  //       .delete(`/api/message/${message._id}`)
  //       .expect(200)
  //       .end(err => done(err));
  //   });
  // });
});
