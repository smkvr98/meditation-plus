import { app } from '../../server.conf.js';
import moment from 'moment-timezone';
import supertest from 'supertest';
import { expect } from 'chai';
import { AuthedSupertest } from '../helper/authed-supertest.js';
import Appointment from '../models/appointment.model.js';
import Meeting from '../models/meeting.model.js';
import Settings from '../models/settings.model.js';
import { User } from '../models/user.model.js';
import timekeeper from 'timekeeper';

const request = supertest(app);
let user = new AuthedSupertest();
let user2 = new AuthedSupertest(
  'Second User',
  'user2',
  'user2@sirimangalo.org',
  'password'
);
let admin = new AuthedSupertest(
  'Admin User',
  'admin',
  'admin@sirimangalo.org',
  'password',
  'ROLE_ADMIN'
);

describe('Appointment Routes', () => {
  let appointment;

  before(async () => {
    await Meeting.remove();
    await User.remove();
    await Appointment.remove();
  });

  beforeEach(async () => {
    await Settings.remove();
    await Appointment.remove();
    appointment = await Appointment.create({
      weekDay: 1,
      hour: 1200
    });
    await Settings.create({ appointmentsTimezone: 'America/Toronto' });
  });

  afterEach(() => {
    return Appointment.remove().exec() & Settings.remove().exec();
  });

  after(async () => await User.remove());

  describe('GET /api/appointment', () => {
    user.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/appointment')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with data when authenticated', done => {
      user
        .get('/api/appointment')
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.have.property('appointments');
          expect(res.body).to.have.property('ownAppointment');
          expect(res.body.appointments.length).to.equal(1);
          expect(res.body.appointments[0].weekDay).to.equal(1);
          expect(res.body.appointments[0].nextDate.length > 0).to.equal(true);
          expect(moment(res.body.appointments[0].nextDate).isValid()).to.equal(true);
          expect(res.body.ownAppointment).to.equal(null);
          done(err);
        });
    });

    it('should respond with more data when added', done => {
      Appointment.create({
        weekDay: 2,
        hour: 700
      }).then((res, err) => {
        if (err) return done(err);

        user
          .get('/api/appointment')
          .expect(200)
          .end((err, res) => {
            expect(res.body).to.have.property('appointments');
            expect(res.body.appointments.length).to.equal(2);
            expect(res.body.appointments[0].nextDate.length > 0).to.equal(true);
            expect(moment(res.body.appointments[0].nextDate).isValid()).to.equal(true);
            expect(res.body.ownAppointment).to.equal(null);
            done(err);
          });
      });
    });
  });

  describe('GET /api/appointment/aggregated', () => {
    user.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/appointment/aggregated')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with data when authenticated', done => {
      user
        .get('/api/appointment/aggregated')
        .expect(200)
        .end((err, res) => {
          expect(res.body.length).to.equal(1);
          expect(res.body[0]._id).to.equal(1200);
          expect(res.body[0].days).to.eql([1]);
          done(err);
        });
    });

    it('should respond with more data when added', done => {
      Appointment.create({
        weekDay: 2,
        hour: 700
      }).then((res, err) => {
        if (err) return done(err);

        user
          .get('/api/appointment/aggregated')
          .expect(200)
          .end((err, res) => {
            expect(res.body.length).to.equal(2);
            expect(res.body[0]._id).to.equal(700);
            expect(res.body[0].days).to.eql([2]);
            expect(res.body[1]._id).to.equal(1200);
            expect(res.body[1].days).to.eql([1]);
            done(err);
          });
      });
    });
  });

  describe('POST /api/appointment/:id/register', () => {
    user.authorize();
    user2.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post(`/api/appointment/${appointment._id}/register`)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 204 on success', done => {
      user
        .post(`/api/appointment/${appointment._id}/register`)
        .expect(204)
        .end(err => {
          if (err) return done(err);
          // deregistration should work, too
          user
            .post(`/api/appointment/${appointment._id}/register`)
            .expect(204)
            .end(err => done(err));
        });
    });

    it('should respond with 400 when already taken', done => {
      user
        .post(`/api/appointment/${appointment._id}/register`)
        .expect(204)
        .end(err => {
          if (err) return done(err);

          // registration of second user should not work
          user2
            .post(`/api/appointment/${appointment._id}/register`)
            .expect(400)
            .end(err => done(err));
        });
    });
  });

  describe('GET /api/appointment/:id', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get(`/api/appointment/${appointment._id}`)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when authenticated as user', done => {
      user
        .get(`/api/appointment/${appointment._id}`)
        .expect(401)
        .end(err => done(err));
    });

    it('should correctly return when authenticated as admin', done => {
      admin
        .get(`/api/appointment/${appointment._id}`)
        .expect(200)
        .end((err, res) => {
          expect(res.body._id).to.equal(appointment._id.toString());
          done(err);
        });
    });
  });

  describe('POST /api/appointment', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/appointment')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when authenticated as user', done => {
      user
        .post('/api/appointment')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should correctly return when authenticated as admin', done => {
      admin
        .post('/api/appointment')
        .send({
          hour: 0,
          weekDay: 0
        })
        .expect(201)
        .end(err => done(err));
    });
  });

  describe('POST /api/appointment/toggle', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/appointment/toggle')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when authenticated as user', done => {
      user
        .post('/api/appointment/toggle')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 when given invalid params', done => {
      admin
        .post('/api/appointment/toggle')
        .send({
          hour: 'invalid'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 201 when trying to add a new appointment', done => {
      admin
        .post('/api/appointment/toggle')
        .send({
          hour: 850,
          day: 3
        })
        .expect(201)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({}, (err, res) => {
              expect(res.length).to.equal(2);
              expect(res[0].hour).to.equal(1200);
              expect(res[0].weekDay).to.equal(1);
              expect(res[1].hour).to.equal(850);
              expect(res[1].weekDay).to.equal(3);

              done(err);
            });
        });
    });

    it('should respond with 204 when trying to delete an existing appointment', done => {
      admin
        .post('/api/appointment/toggle')
        .send({
          hour: 1200,
          day: 1
        })
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({}, (err, res) => {
              expect(res.length).to.equal(0);
              done(err);
            });
        });
    });
  });

  describe('POST /api/appointment/update', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .post('/api/appointment/update')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when authenticated as user', done => {
      user
        .post('/api/appointment/update')
        .send({
          weekDay: 0
        })
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 400 when given invalid params', done => {
      admin
        .post('/api/appointment/update')
        .send({
          oldHour: 'invalid'
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 when appointment already exists', done => {
      admin
        .post('/api/appointment/update')
        .send({
          oldHour: 1200,
          newHour: 1200
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 400 when no appointment with given hour can be found', done => {
      admin
        .post('/api/appointment/update')
        .send({
          oldHour: 500,
          newHour: 600
        })
        .expect(400)
        .end(err => done(err));
    });

    it('should respond with 204 when params are valid', done => {
      admin
        .post('/api/appointment/update')
        .send({
          oldHour: 1200,
          newHour: 300
        })
        .expect(204)
        .end(err => {
          if (err) return done(err);

          Appointment
            .find({}, (err, res) => {
              expect(res.length).to.equal(1);
              expect(res[0].hour).to.equal(300);
              expect(res[0].weekDay).to.equal(1);

              done(err);
            });
        });
    });
  });

  describe('DELETE /api/appointment/remove/:id', () => {
    user.authorize();
    admin.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .delete(`/api/appointment/remove/${appointment._id}`)
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 401 when authenticated as user', done => {
      user
        .delete(`/api/appointment/remove/${appointment._id}`)
        .expect(401)
        .end(err => done(err));
    });

    it('should correctly delete when authenticated as admin', done => {
      admin
        .delete(`/api/appointment/remove/${appointment._id}`)
        .expect(204)
        .end(err => {
          if (err) return done(err);

          // check if really deleted
          admin
            .get(`/api/appointment/${appointment._id}`)
            .expect(200)
            .end((err, res) => {
              expect(res.body.user).to.equal(null);
              done(err);
            });
        });
    });
  });

  describe('GET /api/appointment/meeting', () => {
    user2.authorize();
    user.authorize();

    it('should respond with 401 when not authenticated', done => {
      request
        .get('/api/appointment/meeting')
        .expect(401)
        .end(err => done(err));
    });

    it('should respond with 403 if schedule is disabled', async () => {
      await Settings.updateOne({}, { disableAppointments: true });
      const res = await user2.get('/api/appointment/meeting');
      expect(res.status).to.equal(403);
      expect(res.body.scheduleDisabled).to.equal(true);
      await Settings.updateOne({}, { disableAppointments: false });
    });

    it('should respond with 403 if user has no appointment', async () => {
      await appointment.update({ user: user.user._id });
      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(0)
        .second(0)
        .toDate()
      );
      const res = await user2.get('/api/appointment/meeting');
      expect(res.status).to.equal(403);
      expect(res.body.scheduleDisabled).to.equal(false);
    });

    it('should respond with 403 if user requests at wrong time and there is no meeting', async () => {
      await appointment.update({ user: user.user._id });
      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay+1)
        .hour(12)
        .minute(0)
        .second(0)
        .toDate()
      );
      const res1 = await user.get('/api/appointment/meeting');
      expect(res1.status).to.equal(403);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(0)
        .second(0)
        .toDate()
      );
      const res2 = await user.get('/api/appointment/meeting');
      expect(res2.status).to.equal(403);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(54)
        .second(54)
        .toDate()
      );
      const res3 = await user.get('/api/appointment/meeting');
      expect(res3.status).to.equal(403);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(11)
        .second(0)
        .toDate()
      );
      const res4 = await user.get('/api/appointment/meeting');
      expect(res4.status).to.equal(403);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(30)
        .second(0)
        .toDate()
      );
      const res5 = await user.get('/api/appointment/meeting');
      expect(res5.status).to.equal(403);
    });

    it('should respond with 201 and meeting if user requests at right time', async () => {
      await appointment.update({ user: user.user._id });

      const expectedRes = {
        user: {
          _id: user.user._id.toString(),
          lastMeditation: '1970-01-01T00:00:00.000Z',
          name: 'User',
          username: 'user'
        },
        appointment: appointment._id.toString()
      };

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(55)
        .second(3)
        .format()
      );

      const res1 = await user.get('/api/appointment/meeting');
      expect(res1.status).to.equal(201);
      expect(res1.body.appointment).to.equal(expectedRes.appointment);
      expect(res1.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(57)
        .second(3)
        .toDate()
      );
      await Meeting.remove();
      const res2 = await user.get('/api/appointment/meeting');
      expect(res2.status).to.equal(201);
      expect(res2.body.appointment).to.equal(expectedRes.appointment);
      expect(res2.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(0)
        .second(3)
        .toDate()
      );
      await Meeting.remove();
      const res3 = await user.get('/api/appointment/meeting');
      expect(res3.status).to.equal(201);
      expect(res3.body.appointment).to.equal(expectedRes.appointment);
      expect(res3.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(5)
        .second(3)
        .toDate()
      );
      await Meeting.remove();
      const res4 = await user.get('/api/appointment/meeting');
      expect(res4.status).to.equal(201);
      expect(res4.body.appointment).to.equal(expectedRes.appointment);
      expect(res4.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(9)
        .second(50)
        .toDate()
      );
      await Meeting.remove();
      const res5 = await user.get('/api/appointment/meeting');
      expect(res5.status).to.equal(201);
      expect(res5.body.appointment).to.equal(expectedRes.appointment);
      expect(res5.body.user).to.include(expectedRes.user);
    });

    it('should respond with 200 and meeting if already exists', async () => {
      const expectedRes = {
        user: {
          _id: user.user._id.toString(),
          lastMeditation: '1970-01-01T00:00:00.000Z',
          name: 'User',
          username: 'user'
        },
        appointment: appointment._id.toString()
      };

      await appointment.update({ user: user.user._id });
      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(55)
        .second(0)
        .toDate()
      );

      const res1 = await user.get('/api/appointment/meeting');
      expect(res1.status).to.equal(201);
      expect(res1.body.appointment).to.equal(expectedRes.appointment);
      expect(res1.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(11)
        .minute(57)
        .second(3)
        .toDate()
      );
      const res2 = await user.get('/api/appointment/meeting');
      expect(res2.status).to.equal(200);
      expect(res2.body.appointment).to.equal(expectedRes.appointment);
      expect(res2.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(6)
        .second(0)
        .toDate()
      );
      const res3 = await user.get('/api/appointment/meeting');
      expect(res3.status).to.equal(200);
      expect(res3.body.appointment).to.equal(expectedRes.appointment);
      expect(res3.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(15)
        .second(3)
        .toDate()
      );
      const res4 = await user.get('/api/appointment/meeting');
      expect(res4.status).to.equal(200);
      expect(res4.body.appointment).to.equal(expectedRes.appointment);
      expect(res4.body.user).to.include(expectedRes.user);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(24)
        .second(0)
        .toDate()
      );
      const res5 = await user.get('/api/appointment/meeting');
      expect(res5.status).to.equal(200);
      expect(res5.body.appointment).to.equal(expectedRes.appointment);
      expect(res5.body.user).to.include(expectedRes.user);
    });

    it('should respond with 403 if time exceeded', async () => {
      await appointment.update({ user: user.user._id });
      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(25)
        .second(10)
        .toDate()
      );

      const res1 = await user.get('/api/appointment/meeting');
      expect(res1.status).to.equal(403);

      timekeeper.travel(moment.tz('America/Toronto')
        .weekday(appointment.weekDay)
        .hour(12)
        .minute(40)
        .second(10)
        .toDate()
      );

      const res2 = await user.get('/api/appointment/meeting');
      expect(res2.status).to.equal(403);
    });
  });
});
