import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import Meeting from '../models/meeting.model';
import MeetingMessage from '../models/meeting-message.model';
import moment from 'moment-timezone';
import { logger } from './logger.js';
import { round, get as gv } from 'lodash';

/**
 * Parse appointment hour stored as number
 * into hour and minute.
 */
const parseAppointmentHour = (rawHour) => ({
  hour: Math.floor(rawHour / 100),
  minute: rawHour % 100
});

/**
 * Calculates the minutes remaining minutes to the appointment
 * of the current week.
 *
 * @param {Appointment} appointment
 *
 * @return difference in minutes to the appointment.
 */
const minutesUntilAppointment = async (appointment, nowMoment = moment.utc()) => {
  if (!appointment || typeof appointment.hour !== 'number' || typeof appointment.weekDay !== 'number') {
    return Infinity;
  }

  // load settings entity
  const settings = await Settings.findOne();
  const appointmentsTimezone = settings && !!moment.tz.zone(settings.appointmentsTimezone)
    ? settings.appointmentsTimezone
    : '';

  const nowMomentTz = moment.tz(nowMoment, appointmentsTimezone);

  // create a Moment for appointment
  const { hour, minute } = parseAppointmentHour(appointment.hour);
  let appDateTimeSpecs = {
    weekday: appointment.weekDay,
    hour,
    minute,
    second: 0,
    millisecond: 0
  };

  // treat special cases which occur betwen SATURDAY and SUNDAY
  // because of the indexing from 0-6
  if (nowMomentTz.weekday() === 6 && appointment.weekDay === 0) {
    appDateTimeSpecs.weekday = 7; // now = SATURDAY, appointment = SUNDAY => take tomorror (0 + 7)
  } else if (nowMomentTz.weekday() === 0 && appointment.weekDay === 6) {
    appDateTimeSpecs.weekday = -1; // now = SUNDAY, appointment = SATURDAY => take yesterday (6 - 7)
  }

  const appMoment = nowMomentTz.clone().set(appDateTimeSpecs);
  return round(appMoment.diff(nowMomentTz, 'minutes', true), 2);
};

/**
 * Find an appointment that is scheduled for now for a given user.
 *
 * @param {User}    user               The user to make the lookup for
 * @param {number}  tooEarlyTolerance  Number of minutes before the appointment's scheduled time
 *                                     tolerated for user to initiate.
 * @param {number}  tooLateTolerance   Number of minutes after the appointment's scheduled time
 *                                     tolerated for user to initiate.
 *
 * @return A matching appointment object or null.
 */
const findCurrentAppointmentForUser = async (user, tooEarlyTolerance = 5, tooLateTolerance = 7) => {
  if (!user || !user._id) {
    return null;
  }

  let appointment = await Appointment
    .findOne({ user: user._id })
    .lean();

  const diffMins = await minutesUntilAppointment(appointment);
  return -1 * diffMins <= tooLateTolerance && diffMins <= tooEarlyTolerance
    ? appointment
    : null;
};

/**
 * Check whether the schedule page is set to
 * be disabled for the current moment.
 */
const isScheduleDisabled = async () => {
  try {
    const settings = await Settings.findOne();
    if (!settings || settings.disableAppointments !== true) {
      return false;
    }

    // if no start and end time are set, then the deactivation is treated as permanent
    if (!settings.disableAppointmentsFrom && !settings.disableAppointmentsUntil) {
      return true;
    }

    // check time range
    const tz = settings && !!moment.tz.zone(settings.appointmentsTimezone)
      ? settings.appointmentsTimezone
      : '';

    const momentFrom = moment.tz(settings.disableAppointmentsFrom, 'YYYY-MM-DD', true, tz);
    const momentUntil = moment.tz(settings.disableAppointmentsUntil, 'YYYY-MM-DD', true, tz);

    // in case a date is invalid, the deactivation is treated as invalid, as well
    if (!momentFrom.isValid() || !momentUntil.isValid()) {
      return false;
    }

    const momentNow = moment.tz(settings.appointmentsTimezone);

    return momentNow.isSameOrAfter(momentFrom, 'day') && momentNow.isSameOrBefore(momentUntil, 'day');
  } catch (error) {
    logger.error('Failed to run isScheduleDisabled()', error);
    return false;
  }
};

/**
 * Check whether a user is the one which is the only
 * admin that should able to join meetings.
 *
 * @param {object} user user object to check
 */
const isYuttadhammo = (user) => gv(user, 'role', null) === 'ROLE_ADMIN' && gv(user, 'username') === 'yuttadhammo';

/**
 * Returns the latest message from the latest meeting that
 * the given user participated in.
 *
 * @param {User} user
 */
const getLatestMeetingMessage = async (user) => {
  if (!user || !user._id) {
    return null;
  }

  const latestMeeting = await Meeting
    .find(!isYuttadhammo(user)
      ? { user: user._id }
      : {}
    )
    .sort('-createdAt')
    .limit(1)
    .lean();

  if (latestMeeting.length === 0) {
    return null;
  }

  const latestMeetingMessage = await MeetingMessage
    .find({ meeting: latestMeeting[0]._id })
    .sort('-createdAt')
    .limit(1)
    .lean();

  return latestMeetingMessage.length > 0 ? latestMeetingMessage[0] : null;
};

export {
  parseAppointmentHour,
  minutesUntilAppointment,
  findCurrentAppointmentForUser,
  isScheduleDisabled,
  isYuttadhammo,
  getLatestMeetingMessage
};
