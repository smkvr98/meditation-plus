import mongoose from 'mongoose';

let meetingMessageSchema = mongoose.Schema({
  meeting : { type: mongoose.Schema.Types.ObjectId, ref: 'Meeting', required: true },
  text : { type: String, required: true, maxlength: 1000 },
  user : { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  first: Boolean,
  edited: Boolean,
  deleted: Boolean  
}, {
  timestamps: true
});

export default mongoose.model('MeetingMessage', meetingMessageSchema);
