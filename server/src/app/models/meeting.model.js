import mongoose from 'mongoose';

let MeetingSchema = mongoose.Schema({
  user: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  appointment: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'Appointment',
    required: true
  },
  initiated: { type: Boolean, default: false, required: true }, // deprecated, but kept for compatibility reasons
  discordLink: { type: String } // deprecated, but kept for compatibility reasons
}, {
  timestamps: true
});

export default mongoose.model('Meeting', MeetingSchema);
