import mongoose from 'mongoose';

let settingsSchema = mongoose.Schema({
  appointmentsTimezone: {
    type: String,
    default: 'America/Toronto'
  },
  disableAppointments: Boolean,
  disableAppointmentsFrom: String,
  disableAppointmentsUntil: String,
  livestreamInfo: String,
  notification: String
});

export default mongoose.model('Settings', settingsSchema);
