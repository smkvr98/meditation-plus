import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { SharedModule } from '../shared';
import { ProfileModule } from '../profile';
import { AppointmentComponent } from './appointment.component';

const routes: Routes = [
  { path: '', component: AppointmentComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ProfileModule,
    FormsModule,
    ReactiveFormsModule,
    MomentModule
  ],
  declarations: [
    AppointmentComponent
  ],
  exports: [
    AppointmentComponent
  ]
})
export class AppointmentModule { }
