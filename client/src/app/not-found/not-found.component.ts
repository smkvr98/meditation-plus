import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle } from '../actions/global.actions';

@Component({
  selector: 'not-found',
  template: `
    <mat-card>
      <mat-card-title>Page has not been found</mat-card-title>
      <p>The page you are looking for does not exist.</p>
    </mat-card>
  `
})
export class NotFoundComponent {
  constructor(store: Store<AppState>) {
    store.dispatch(new SetTitle('Not found'));
  }
}
