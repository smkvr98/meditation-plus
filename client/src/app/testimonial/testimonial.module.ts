import { UserTextListModule } from './../user-text-list/user-text-list.module';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared';
import { TestimonialComponent } from './testimonial.component';
import { EmojiModule, EmojiSelectComponent } from '../emoji';
import { ProfileModule } from '../profile';

const routes: Routes = [
  { path: '', component: TestimonialComponent }
];

@NgModule({
  imports: [
    SharedModule,
    ProfileModule,
    FormsModule,
    RouterModule.forChild(routes),
    EmojiModule,
    UserTextListModule
  ],
  declarations: [
    TestimonialComponent
  ],
  entryComponents: [
    EmojiSelectComponent
  ],
  exports: [
    TestimonialComponent
  ]
})
export class TestimonialModule { }
