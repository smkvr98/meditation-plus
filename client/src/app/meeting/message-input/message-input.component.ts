import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { selectPostingMessage } from '../reducers/meeting.reducers';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';
import { PostMeetingMessage, POST_MESSAGE_DONE, PostMeetingMessageDone } from '../actions/meeting.actions';
import { AppState } from '../../reducers';
import { Actions } from '@ngrx/effects';
import { MatBottomSheet } from '../../../../node_modules/@angular/material';
import { EmojiSelectComponent } from '../../emoji';

@Component({
  selector: 'message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.styl'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageInputComponent {
  @Input() meetingId: string;
  form: FormGroup;
  message: FormControl;

  posting$: Observable<boolean>;

  constructor(
    private store: Store<AppState>,
    private sheet: MatBottomSheet,
    actions$: Actions
  ) {
    this.message = new FormControl();
    this.form = new FormGroup({
      message: this.message
    });

    this.posting$ = store.select(selectPostingMessage);

    this.posting$.subscribe(val => val
      ? this.message.disable()
      : this.message.enable()
    );

    actions$.ofType<PostMeetingMessageDone>(POST_MESSAGE_DONE)
      .subscribe(() => this.message.setValue(''));
   }

  showEmojiSelect() {
    this.sheet.open(EmojiSelectComponent).afterDismissed()
      .pipe(filter(val => !!val))
      .subscribe(val => this.message.setValue(`${this.message.value || ''}:${val}:`));
  }

  sendMessage(evt: KeyboardEvent) {
    evt.preventDefault();
    if (!this.message.value.trim()) {
      return;
    }

    this.store.dispatch(new PostMeetingMessage({ meetingId: this.meetingId, message: this.message.value}));
  }

  autocomplete(evt: KeyboardEvent) {
    evt.preventDefault();
  }
}
