import { MeetingMessage } from 'app/message/message';
import { Meeting } from '../meeting';
import * as meeting from '../actions/meeting.actions';
import * as moment from 'moment';
import * as _ from 'lodash';
import { AppState } from 'app/reducers';
import { createSelector } from '@ngrx/store';

export interface MeetingState {
  messages: MeetingMessage[];
  noPagesLeft: boolean;
  loadedPage: number;
  loadingChat: boolean;
  initiallyLoadedChat: boolean;
  postingMessage: boolean;
  meeting: Meeting;
  meetingPageDisabled: boolean;
}

export const initialMeetingState: MeetingState = {
  messages: [],
  noPagesLeft: false,
  loadedPage: 0,
  loadingChat: false,
  initiallyLoadedChat: false,
  postingMessage: false,
  meeting: null,
  meetingPageDisabled: false
};

export function meetingReducer(
  state = initialMeetingState,
  action: meeting.Actions
): MeetingState {
  switch (action.type) {
    case meeting.LOAD_MEETING_DONE: {
      return {
        ..._.cloneDeep(state),
        meeting: action.payload,
        meetingPageDisabled: false
      };
    }

    case meeting.LOAD_MEETING_ERROR: {
      const meetingPageDisabled = _.get(action.payload, 'status', 0) === 403
        && _.get(action.payload, 'error.scheduleDisabled', false) === true;

      return {
        ..._.cloneDeep(state),
        meetingPageDisabled
      };
    }

    case meeting.LOAD_MESSAGES: {
      return {
        ..._.cloneDeep(state),
        loadingChat: true
      };
    }

    case meeting.LOAD_MESSAGES_DONE: {
      return loadDone(state, action);
    }

    case meeting.POST_MESSAGE: {
      return {..._.cloneDeep(state), postingMessage: true};
    }

    case meeting.POST_MESSAGE_DONE: {
      return {..._.cloneDeep(state), postingMessage: false};
    }

    case meeting.SYNC_MESSAGES_DONE: {
      const newState = _.cloneDeep(state);
      newState.messages.splice(
        action.payload.index,
        0,
        ...action.payload.messages
      );

      return {
        ...newState,
        messages: newState.messages.sort(sortMessages)
      };
    }

    case meeting.WS_ON_MESSAGE: {
      return {
        ..._.cloneDeep(state),
        messages: [...state.messages, action.payload.current]
          .sort(sortMessages)
      };
    }

    case meeting.WS_ON_UPDATE:
    case meeting.UPDATE_MESSAGE: {
      return {
        ..._.cloneDeep(state),
        messages: state.messages.map(val => {
          if (val._id === action.payload._id) {
            return action.payload;
          }

          return val;
        })
      };
    }

    default: {
      return state;
    }
  }
}

// Extracted more complex reducer functions
function loadDone(state: MeetingState, action: meeting.LoadMeetingMessagesDone): MeetingState {
  const messages = state.initiallyLoadedChat
    ? [...action.payload.messages, ..._.cloneDeep(state.messages)]
    : action.payload.messages;

  return {
    ..._.cloneDeep(state),
    messages,
    loadedPage: action.payload.page,
    noPagesLeft: action.payload.page > 0 && action.payload.messages.length === 0,
    loadingChat: false,
    initiallyLoadedChat: true
  };
}

// Helper functions
function sortMessages(a: any, b: any) {
  return moment(a.createdAt).unix() - moment(b.createdAt).unix();
}

// Selectors for easy access
export const selectMeeting = (state: AppState) => state.meeting;
export const selectCurrentMeeting = createSelector(selectMeeting, (state: MeetingState) => state.meeting);
export const selectMeetingPageDisabled = createSelector(selectMeeting, (state: MeetingState) => state.meetingPageDisabled);
export const selectMeetingMessageList = createSelector(selectMeeting, (state: MeetingState) => state.messages);
export const selectNoPagesLeft = createSelector(selectMeeting, (state: MeetingState) => state.noPagesLeft);
export const selectLoadedPage = createSelector(selectMeeting, (state: MeetingState) => state.loadedPage);
export const selectLoadingChat = createSelector(selectMeeting, (state: MeetingState) => state.loadingChat);
export const selectPostingMessage = createSelector(selectMeeting, (state: MeetingState) => state.postingMessage);
export const selectInitiallyLoadedChat = createSelector(selectMeeting, (state: MeetingState) => state.initiallyLoadedChat);
