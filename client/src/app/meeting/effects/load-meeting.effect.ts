import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { concatMap, map, catchError } from 'rxjs/operators';
import { AppointmentService } from 'app/appointment/appointment.service';
import {
  LOAD_MEETING,
  LoadMeeting,
  LoadMeetingDone,
  LoadMeetingError
} from '../actions/meeting.actions';
import { of } from 'rxjs';

@Injectable()
export class LoadMeetingEffect {
  constructor(
    private actions$: Actions,
    private service: AppointmentService
  ) {
  }

  @Effect()
  loadMeeting$ = this.actions$
    .ofType<LoadMeeting>(LOAD_MEETING)
    .pipe(
      concatMap(() =>
        this.service.getMeeting().pipe(
          map(meeting => new LoadMeetingDone(meeting)),
          catchError(error => of(new LoadMeetingError(error)))
        )
      )
    );
}
