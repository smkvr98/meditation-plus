import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { MeetingService } from 'app/meeting/meeting.service';
import { WebsocketOnUpdateMessage } from 'app/meeting/actions/meeting.actions';
import { of } from 'rxjs';
import { concatMap } from 'rxjs/operators';

@Injectable()
export class WsOnUpdateMeetingMessageEffect {
  constructor(
    private service: MeetingService
  ) {
  }

  @Effect()
  wsOnConnect$ = this.service.getUpdateSocket()
    .pipe(
      concatMap(data => of(new WebsocketOnUpdateMessage(data.populated)))
    );
}
