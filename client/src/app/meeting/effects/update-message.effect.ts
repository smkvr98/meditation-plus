import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MeetingService } from 'app/meeting/meeting.service';
import { concatMap, map } from 'rxjs/operators';
import { UPDATE_MESSAGE, UpdateMeetingMessage } from '../actions/meeting.actions';

@Injectable()
export class UpdateMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MeetingService
  ) {
  }

  @Effect({ dispatch: false })
  post$ = this.actions$
    .ofType<UpdateMeetingMessage>(UPDATE_MESSAGE)
    .pipe(
      map(action => action.payload),
      concatMap(payload => this.service.updateMessage(payload))
    );
}
