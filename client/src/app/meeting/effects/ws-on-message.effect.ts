import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { AppState } from 'app/reducers';
import {
  WS_ON_MESSAGE,
  WebsocketOnMeetingMessage,
  SyncMeetingMessages,
  WebsocketOnMeetingMessagePayload
} from 'app/meeting/actions/meeting.actions';
import { map, withLatestFrom, concatMap } from 'rxjs/operators';
import * as moment from 'moment';
import { selectMeetingMessageList } from 'app/meeting/reducers/meeting.reducers';
import { MeetingMessage } from 'app/message/message';
import { MeetingService } from 'app/meeting/meeting.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class WSOnMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private store$: Store<AppState>,
    private service: MeetingService
  ) {
  }

  @Effect()
  wsReceiverMessage$ = this.service.getMessageSocket()
    .pipe(map(data => new WebsocketOnMeetingMessage(data)));

  @Effect()
  wsOnMessage$ = this.actions$
    .ofType<WebsocketOnMeetingMessage>(WS_ON_MESSAGE)
    .pipe(
      map(action => action.payload),
      withLatestFrom(this.store$.select(selectMeetingMessageList)),
      concatMap(([payload, messages]) => mapOnMessageToSync(payload, messages))
    );
}

export function mapOnMessageToSync(payload: WebsocketOnMeetingMessagePayload, messages: MeetingMessage[]): Observable<Action> {
  const last = messages && messages.length > 1 ? messages[messages.length - 2] : null;

  if (!last || !payload.previous || moment(last.createdAt).isSame(payload.previous.createdAt)) {
    return of({ type: 'NO_ACTION' });
  }

  return of(new SyncMeetingMessages({
    index: messages.length - 2,
    from: last.createdAt,
    to: moment(payload.previous.createdAt)
  }));
}
