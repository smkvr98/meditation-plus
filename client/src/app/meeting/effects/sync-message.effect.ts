import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MeetingService } from 'app/meeting/meeting.service';
import { concatMap, map } from 'rxjs/operators';
import {
  SyncMeetingMessages,
  SYNC_MESSAGES,
  SyncMeetingMessagesDone
} from '../actions/meeting.actions';
import { of } from 'rxjs';
import * as moment from 'moment';

@Injectable()
export class SyncMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MeetingService,
  ) {
  }

  @Effect()
  sync$ = this.actions$
    .ofType<SyncMeetingMessages>(SYNC_MESSAGES)
    .pipe(
      map(action => action.payload),
      concatMap(({ from, to, index }) =>
        this.service.synchronizeMessage(moment(from), to).pipe(
          concatMap(messages => of(new SyncMeetingMessagesDone({ index: index, messages })))
        )
      )
    );
}
