import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MeetingMessage } from 'app/message/message';
import { MeetingService } from 'app/meeting/meeting.service';
import { concatMap, map, tap } from 'rxjs/operators';
import * as _ from 'lodash';
import { LOAD_MESSAGES, LoadMeetingMessages, LoadMeetingMessagesDone } from 'app/meeting/actions/meeting.actions';
import { of } from 'rxjs';

@Injectable()
export class LoadMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MeetingService
  ) {
  }

  @Effect()
  load$ = this.actions$
    .ofType<LoadMeetingMessages>(LOAD_MESSAGES)
    .pipe(
      map(action => action.payload),
      concatMap(({ page }) =>
        this.service.getMessages(page).pipe(
          tap(this.setLastMessage),
          concatMap(messages => of(new LoadMeetingMessagesDone({ messages, page })))
        )
      )
    );

  private setLastMessage(messages: MeetingMessage[]) {
   if (messages.length === 0) {
     return;
   }
   const lastMessage = _.last(messages);
   window.localStorage.setItem(
     'lastMeetingMessage',
     lastMessage.createdAt.toString()
   );
   window.localStorage.setItem(
     'lastMeetingMessageId',
     lastMessage._id
   );
  }
}
