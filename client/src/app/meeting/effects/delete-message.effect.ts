import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MeetingService } from 'app/meeting/meeting.service';
import { concatMap, map } from 'rxjs/operators';
import { DELETE_MESSAGE, DeleteMeetingMessage, DeleteMeetingMessageDone } from '../actions/meeting.actions';

@Injectable()
export class DeleteMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MeetingService
  ) {
  }

  @Effect()
  post$ = this.actions$
    .ofType<DeleteMeetingMessage>(DELETE_MESSAGE)
    .pipe(
      map(action => action.payload),
      concatMap(payload => this.service.deleteMessage(payload)),
      map(() => new DeleteMeetingMessageDone())
    );
}
