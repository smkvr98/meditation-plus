import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { MeetingService } from 'app/meeting/meeting.service';
import { concatMap, map } from 'rxjs/operators';
import {
  PostMeetingMessage,
  POST_MESSAGE,
  PostMeetingMessageDone
} from '../actions/meeting.actions';
import { of } from 'rxjs';

@Injectable()
export class PostMeetingMessageEffect {
  constructor(
    private actions$: Actions,
    private service: MeetingService
  ) {
  }

  @Effect()
  post$ = this.actions$
    .ofType<PostMeetingMessage>(POST_MESSAGE)
    .pipe(
      map(action => action.payload),
      concatMap(({ meetingId, message }) => this.service.postMessage(meetingId, message)),
      concatMap(() => of(new PostMeetingMessageDone()))
    );
}
