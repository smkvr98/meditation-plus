import { Action } from '@ngrx/store';
import { Meeting } from '../meeting';
import { MeetingMessage } from 'app/message/message';
import { Moment } from 'moment';

export const LOAD_MEETING = '[Meeting] Load Meeting';
export const LOAD_MEETING_ERROR = '[Meeting] Load Meeting Error';
export const LOAD_MEETING_DONE = '[Meeting] Load Meeting Done';
export const LOAD_MESSAGES = '[Meeting] Load Messages';
export const LOAD_MESSAGES_ERROR = '[Meeting] Load Messages Error';
export const LOAD_MESSAGES_DONE = '[Meeting] Load Messages Done';
export const POST_MESSAGE = '[Meeting] Post Message';
export const POST_MESSAGE_ERROR = '[Meeting] Post Message Error';
export const POST_MESSAGE_DONE = '[Meeting] Post Message Done';
export const DELETE_MESSAGE = '[Meeting] Delete Message';
export const DELETE_MESSAGE_ERROR = '[Meeting] Delete Message Error';
export const DELETE_MESSAGE_DONE = '[Meeting] Delete Message Done';
export const SYNC_MESSAGES = '[Meeting] Sync Messages';
export const SYNC_MESSAGES_ERROR = '[Meeting] Sync Messages Error';
export const SYNC_MESSAGES_DONE = '[Meeting] Sync Messages Done';
export const WS_ON_MESSAGE = '[Meeting] WS On Message';
export const WS_ON_CONNECT = '[Meeting] WS On Connect';
export const WS_ON_UPDATE = '[Meeting] WS On Update';
export const UPDATE_MESSAGE = '[Meeting] Update Message';
export const UPDATE_MESSAGE_ERROR = '[Meeting] Update Message Error';

export class LoadMeeting implements Action {
  readonly type = LOAD_MEETING;
}

export class LoadMeetingError implements Action {
  readonly type = LOAD_MEETING_ERROR;
  constructor(public payload: object) {}
}

export class LoadMeetingDone implements Action {
  readonly type = LOAD_MEETING_DONE;
  constructor(public payload: Meeting) {}
}

export class LoadMeetingMessages implements Action {
  readonly type = LOAD_MESSAGES;
  constructor(public payload: { page: number }) {}
}

export class LoadMeetingMessagesDone implements Action {
  readonly type = LOAD_MESSAGES_DONE;
  constructor(public payload: { messages: MeetingMessage[], page: number }) {}
}

export class PostMeetingMessage implements Action {
  readonly type = POST_MESSAGE;
  constructor(public payload: { meetingId: string, message: string }) {}
}

export class PostMeetingMessageDone implements Action {
  readonly type = POST_MESSAGE_DONE;
}

export class DeleteMeetingMessage implements Action {
  readonly type = DELETE_MESSAGE;
  constructor(public payload: MeetingMessage) {}
}

export class DeleteMeetingMessageDone implements Action {
  readonly type = DELETE_MESSAGE_DONE;
}

export class SyncMeetingMessages implements Action {
  readonly type = SYNC_MESSAGES;
  constructor(public payload: { index: number, from: Date, to: Moment }) {}
}

export class SyncMeetingMessagesDone implements Action {
  readonly type = SYNC_MESSAGES_DONE;
  constructor(public payload: { index: number, messages: MeetingMessage[]}) {}
}

export interface WebsocketOnMeetingMessagePayload {
  previous: MeetingMessage;
  current: MeetingMessage;
}

export class WebsocketOnMeetingMessage implements Action {
  readonly type = WS_ON_MESSAGE;
  constructor(public payload: WebsocketOnMeetingMessagePayload) {}
}

export interface WebsocketOnConnectPayload {
  latestMessage: MeetingMessage;
}

export class WebsocketOnConnect implements Action {
  readonly type = WS_ON_CONNECT;
  constructor(public payload: WebsocketOnConnectPayload) {}
}

export class UpdateMeetingMessage implements Action {
  readonly type = UPDATE_MESSAGE;
  constructor(public payload: MeetingMessage) {}
}

export class WebsocketOnUpdateMessage implements Action {
  readonly type = WS_ON_UPDATE;
  constructor(public payload: MeetingMessage) {}
}

export type Actions = LoadMeeting
  | LoadMeetingDone
  | LoadMeetingError
  | LoadMeetingMessages
  | LoadMeetingMessagesDone
  | PostMeetingMessage
  | PostMeetingMessageDone
  | DeleteMeetingMessage
  | DeleteMeetingMessageDone
  | SyncMeetingMessages
  | SyncMeetingMessagesDone
  | WebsocketOnMeetingMessage
  | WebsocketOnConnect
  | WebsocketOnUpdateMessage
  | UpdateMeetingMessage;
