import {
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  OnDestroy,
  Output,
  ViewChild
} from '@angular/core';
import { Meeting } from './meeting';
import { MeetingMessage } from '../message/message';
import { Observable, Subscription, interval } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'app/reducers';
import {
  selectMeetingMessageList,
  selectLoadingChat,
  selectLoadedPage,
  selectNoPagesLeft,
  selectInitiallyLoadedChat
} from 'app/meeting/reducers/meeting.reducers';
import {
  selectCurrentMeeting
} from './reducers/meeting.reducers';
import { take, filter, map, tap } from 'rxjs/operators';
import { NgZone } from '@angular/core';
import {
  LoadMeetingMessages,
  UpdateMeetingMessage,
  DeleteMeetingMessage
} from 'app/meeting/actions/meeting.actions';
import {
  LoadMeeting
} from './actions/meeting.actions';
import { LoadMeetingMessageEffect } from './effects/load-messages.effect';
import { selectAdmin } from '../auth/reducders/auth.reducers';
import * as moment from 'moment-timezone';

@Component({
  selector: 'meeting',
  templateUrl: './meeting.component.html',
  styleUrls: [
    './meeting.component.styl'
  ]
})
export class MeetingComponent implements OnInit, OnDestroy {
  @Output() loadingFinished: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('messageList', {read: ElementRef}) messageList: ElementRef;

  messages$: Observable<MeetingMessage[]>;
  meeting$: Observable<Meeting>;
  loadingChat$: Observable<boolean>;
  admin$: Observable<boolean>;
  initiallyLoadedChat$: Observable<boolean>;
  page$: Observable<number>;
  noPagesLeft$: Observable<boolean>;
  lastScrollTop = 0;
  lastScrollHeight = 0;
  checkMeetingInterval: Subscription;

  constructor(
    private store: Store<AppState>,
    private zone: NgZone,
    loadMessageEffect: LoadMeetingMessageEffect,
  ) {
    this.messages$ = store.select(selectMeetingMessageList);
    this.meeting$ = store.select(selectCurrentMeeting);
    this.loadingChat$ = store.select(selectLoadingChat);
    this.initiallyLoadedChat$ = store.select(selectInitiallyLoadedChat);
    this.page$ = store.select(selectLoadedPage);
    this.noPagesLeft$ = store.select(selectNoPagesLeft);
    this.admin$ = store.select(selectAdmin);

    // Load first page, if no messages have been loaded
    store.select(selectMeetingMessageList).pipe(
      take(1),
      filter(messages => messages.length === 0)
    )
    .subscribe(() => store.dispatch(new LoadMeetingMessages({ page: 0 })));

    // scroll to last message when clicking on "load more"
    loadMessageEffect.load$
      .pipe(
        map(() => localStorage.getItem('lastMeetingMessageId')),
        filter(val => !!val),
        map(id => document.getElementById('message-' + id)),
        tap(() =>
          Array.from(document.querySelectorAll('load-more-separator')).forEach(el => el.classList.remove('load-more-separator'))
        ),
        tap(el => el.classList.add('load-more-separator'))
      )
      .subscribe(el => el.scrollIntoView());

      this.store.dispatch(new LoadMeeting());
      this.meeting$.subscribe(meeting => {
        if (meeting) {
          if (this.checkMeetingInterval) {
            this.checkMeetingInterval.unsubscribe();
            this.checkMeetingInterval = null;
          }
        } else if (!this.checkMeetingInterval) {
          this.checkMeetingInterval = interval(4000)
            .subscribe(() => this.store.dispatch(new LoadMeeting()));
        }
      });
  }

  ngOnInit() {
    this.registerScrolling();

    this.messages$
      .pipe(
        filter(() => this.lastScrollTop + 5 >= this.lastScrollHeight
        - this.messageList.nativeElement.offsetHeight)
      )
      .subscribe(() => this.scrollToBottom());

    this.loadingFinished.emit();
  }

  ngOnDestroy() {
    if (this.checkMeetingInterval) {
      this.checkMeetingInterval.unsubscribe();
    }
  }

  /**
   * Converts number representing hour (like 1300) to
   * string in the time format HH:mm ('13:00')
   */
  formatHour(hour: number): string {
    const hourStr = '0000' + hour.toString();
    return hourStr.substr(-4, 2) + ':' + hourStr.substr(-2, 2);
  }

  formatMeetingDate(meeting): string {
    return moment(meeting.createdAt).format('LLLL');
  }

  delete(message: MeetingMessage) {
    this.store.dispatch(new DeleteMeetingMessage(message));
  }

  update(message: MeetingMessage) {
    this.store.dispatch(new UpdateMeetingMessage(message));
  }

  loadNextPage() {
    this.page$.pipe(take(1))
      .subscribe(page => this.store.dispatch(new LoadMeetingMessages({ page: page + 1 })));
  }

  /**
   * Registers scrolling as observable. Running this outside of zone to ignore
   * a change detection run on every scroll event. This resulted in a huge performance boost.
   */
  registerScrolling() {
    this.zone.runOutsideAngular(() => {
      let scrollTimer = null;
      this.messageList.nativeElement.addEventListener('scroll', () => {
        if (scrollTimer !== null) {
          clearTimeout(scrollTimer);
        }
        scrollTimer = setTimeout(() => {
          this.lastScrollHeight = this.messageList.nativeElement.scrollHeight;
          this.lastScrollTop = this.messageList.nativeElement.scrollTop;
        }, 150);
      }, false);
    });
  }

  public scrollToBottom() {
    window.setTimeout(() => {
      this.messageList.nativeElement.scrollTop = this.messageList.nativeElement.scrollHeight;
    }, 10);
  }

  trackById(_index, item: MeetingMessage) {
    return item._id;
  }
}
