import { UserTextListModule } from '../user-text-list/user-text-list.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared';
import { MeetingComponent } from './meeting.component';
import { MeetingMessageListEntryComponent } from 'app/meeting/list-entry/message-list-entry.component';
import { EmojiModule, EmojiSelectComponent } from '../emoji';
import { ProfileModule } from '../profile';
import { MomentModule } from 'ngx-moment';
import { EffectsModule } from '@ngrx/effects';
import { LoadMeetingEffect } from 'app/meeting/effects/load-meeting.effect';
import { LoadMeetingMessageEffect } from 'app/meeting/effects/load-messages.effect';
import { SyncMeetingMessageEffect } from 'app/meeting/effects/sync-message.effect';
import { WSOnMeetingMessageEffect } from 'app/meeting/effects/ws-on-message.effect';
import { PostMeetingMessageEffect } from 'app/meeting/effects/post-message.effect';
import { DeleteMeetingMessageEffect } from 'app/meeting/effects/delete-message.effect';
import { UpdateMeetingMessageEffect } from 'app/meeting/effects/update-message.effect';
import { WsOnUpdateMeetingMessageEffect } from 'app/meeting/effects/ws-on-update-message.effect';
import { MessageInputComponent } from './message-input/message-input.component';

const routes: Routes = [
  { path: '', component: MeetingComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    ProfileModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    EmojiModule,
    MomentModule,
    UserTextListModule,
    EffectsModule.forFeature([
      LoadMeetingEffect,
      LoadMeetingMessageEffect,
      DeleteMeetingMessageEffect,
      UpdateMeetingMessageEffect,
      PostMeetingMessageEffect,
      SyncMeetingMessageEffect,
      WSOnMeetingMessageEffect,
      WsOnUpdateMeetingMessageEffect
    ]),
  ],
  declarations: [
    MeetingComponent,
    MessageInputComponent,
    MeetingMessageListEntryComponent
  ],
  entryComponents: [
    EmojiSelectComponent
  ],
  exports: [
    MeetingComponent,
    MeetingMessageListEntryComponent
  ]
})
export class MeetingModule { }
