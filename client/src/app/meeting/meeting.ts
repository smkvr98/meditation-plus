import { Appointment } from 'app/appointment/appointment';

export interface Meeting {
  _id: string;
  user: any;
  appointment: Appointment;
  createdAt?: Date;
  updatedAt?: Date;
  initiated?: boolean; // deprecated
  discordLink?: string; // deprecated
}
