import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { WebsocketService } from '../shared';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { concatMap } from 'rxjs/operators';
import { MeetingMessage } from 'app/message/message';

@Injectable()
export class MeetingService {

  public constructor(
    private http: HttpClient,
    private wsService: WebsocketService
  ) {
  }

  public getMessages(page = 0): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', '' + page);

    return this.http.get(`${ApiConfig.url}/api/meeting-message`, { params });
  }

  public postMessage(meetingId: string, message: string): Observable<any> {
    return this.http.post(
      `${ApiConfig.url}/api/meeting-message/${meetingId}`,
      JSON.stringify({ text: message })
    );
  }

  public updateMessage(message: MeetingMessage): Observable<any> {
    return this.http.put(
      `${ApiConfig.url}/api/meeting-message/${message._id}/${message.meeting}`,
      JSON.stringify({ text: message.text })
    );
  }

  public deleteMessage(message: MeetingMessage): Observable<any> {
    return this.http.delete(
      `${ApiConfig.url}/api/meeting-message/${message._id}/${message.meeting}`
    );
  }

  public synchronizeMessage(
    timeFrameStart: moment.Moment,
    timeFrameEnd: moment.Moment,
    countOnly: Boolean = false
  ): Observable<any> {
    return this.http.post(
      `${ApiConfig.url}/api/meeting-message/synchronize`,
      JSON.stringify({ timeFrameStart, timeFrameEnd, countOnly})
    );
  }

  public getMessageSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('meeting-message', res => obs.next(res));
      }))
    );
  }

  public getUpdateSocket(): Observable<any> {
    return this.wsService.getSocket().pipe(
      concatMap(websocket => Observable.create(obs => {
        websocket.on('meeting-message-update', res => obs.next(res));
      }))
    );
  }

  /**
   * Save the datetime last received message to local storage
   */
  public setLastMessage(messageDate) {
    window.localStorage.setItem('lastMeetingMessage', messageDate);
  }

  /**
   * Get the datetime last received message from local storage
   */
  public getLastMessage(): string {
    return window.localStorage.getItem('lastMeetingMessage');
  }
}
