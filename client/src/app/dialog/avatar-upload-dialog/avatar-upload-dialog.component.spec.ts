import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarUploadDialogComponent } from './avatar-upload-dialog.component';
import { MaterialModule } from '../../shared/material.module';
import { FormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { UserService } from '../../user/user.service';
import { FakeUserService } from '../../user/testing/fake-user.service';

describe('AvatarUploadDialogComponent', () => {
  let component: AvatarUploadDialogComponent;
  let fixture: ComponentFixture<AvatarUploadDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvatarUploadDialogComponent ],
      imports: [ MaterialModule, FormsModule, NoopAnimationsModule ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        {provide: UserService, useClass: FakeUserService},
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarUploadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
